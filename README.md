# keyoxide-web-revamped

## Developing

```bash
# Install the dependencies
yarn

# Run the server
yarn dev
```

## Building

```bash
# Build the project
yarn run build

# Preview the build
yarn run preview
```

## Building the container

```bash
# Build the container image
docker build -t kwr:latest ./

# Run the container image
docker run -p 3000:3000 -d kwr:latest
```